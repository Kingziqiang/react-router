### 1. React Router 中的组件主要分三类:

- 路由器: BrowserRouter 和 HashRouter
- 路由匹配器: Route 和 Switch
- 导航: Link, NavLink 和 Redirect

### 2. BrowserRouter 和 HashRouter 的区别

- BrowserRouter 使用常规 URL 路径。外观好看，但是它要求服务器配置正确（web 服务器需要在所有由 React Router 客户端管理的 URL 上提供相应的页面）
- HashRouter 将当前位置存储在 URL 的哈希部分中，由于哈希从不发送到服务器，因此不需要特殊的服务器配置
