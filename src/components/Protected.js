import React from 'react';
import {Route,Redirect} from '../react-router-dom';
export default function(props){
  let {path, component:RouteComponent} = props;
  return <Route path={path} render={
    props => (
        localStorage.getItem('logined') ? <RouteComponent {...props} />
            : <Redirect to={{pathname: '/login', state: {from: props.location.pathname}}} />
    )
} />
}