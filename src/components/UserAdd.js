import React, { Component } from 'react'
import { Prompt } from '../react-router-dom';
export default class UserAdd extends Component {
  state = {isBlocking: false}; // 是否阻止跳转
  constructor(){
    super();
    this.usernameRef = React.createRef();
  }
  handleSubmit = (event)=>{
    event.preventDefault();
    this.setState({isBlocking: false}, ()=>{
      let username = this.usernameRef.current.value;
      let usersStr = localStorage.getItem('users');
      let users = usersStr?JSON.parse(usersStr):[];
      users.push({id:Date.now()+'',username});
      localStorage.setItem('users',JSON.stringify(users));
      this.props.history.push('/user/list');
    })
  }
  render() {

    let {isBlocking} = this.state;
    return  <form onSubmit={this.handleSubmit}>
    <Prompt
        when={isBlocking}
        message={location => `请问你是否确定要跳转到${location.pathname}?`}
    />
    <input type="text" ref={this.usernameRef} onChange={
        event => this.setState({ isBlocking: event.target.value.length > 0 })
    } />
    <button type="submit">添加</button>
</form>
  }
}
