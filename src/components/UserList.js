import React from 'react';
import { Link } from '../react-router-dom';
import { Users } from '../utils';
export default class extends React.Component {
    state = { users: [] }
    componentDidMount() {
        let users = Users.list();
        this.setState({ users });
    }
    render() {
        return (
            <ul>
                {
                    this.state.users.map(user => (
                        <li key={user.id}>
                            <Link to={{ pathname: `/user/detail/${user.id}`, state: user }}>{user.username}</Link>
                        </li>
                    ))
                }
            </ul>
        )
    }
}