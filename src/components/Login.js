import React, { Component } from 'react'

export default class Login extends Component {
  handleClick = ()=>{
    localStorage.setItem('logined','true');
    console.log(this.props);
    if(this.props.location.state){
      this.props.history.push(this.props.location.state.from);
    }
  }  
  render() {
    return (
      <>
      <hr/>
      <button className="btn btn-primary" onClick={this.handleClick}>登录</button>
      </>
    )
  }
}
