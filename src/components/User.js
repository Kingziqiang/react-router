import React,{Component} from 'react';
import { Route, Link, Switch, Redirect, NavLink } from '../react-router-dom';
import UserList from './UserList';
import UserAdd from './UserAdd';
import UserDetail from './UserDetail';
export default class extends Component {
    render() {
        return (
            <div>
                <ul>
                    <li><NavLink to="/user/list">用户列表</NavLink></li>
                    <li><NavLink to="/user/add">添加用户</NavLink></li>
                </ul>
                <Switch>
                    <Route path="/user/list" component={UserList} />
                    <Route path="/user/add" component={UserAdd} />
                    <Route path="/user/detail/:id" component={UserDetail} />
                    <Redirect to="/user/list" />
                </Switch>
            </div>
        )
    }
}