import React, {Component} from 'react';
import Context from './context';
import Link from './Link';
import Route from './Route';
import {pathToRegexp} from 'path-to-regexp'
/**
 * component 就是要渲染的组件 匹配才渲染
 * render 要渲染的函数匹配才渲染
 * children 就是一个函数， 不管匹配还是不匹配都会渲染
*/
export default class NavLink extends Component {
  static contextType = Context;
  render(){
    let {to, exact = false} = this.props;
    
    return (
      <Route path={typeof to==='object'?to.pathname: to } exact={exact} children={
        (props)=>{
          return <Link to={to} className={props.match? 'active': ''} >
          {this.props.children}
       </Link>
        }
      }></Route>
    )
  }
}