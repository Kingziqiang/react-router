import React, {Component} from 'react';
import Context from './context';
import Link from './Link';
import {pathToRegexp} from 'path-to-regexp'
/**
 * component 就是要渲染的组件 匹配才渲染
 * render 要渲染的函数匹配才渲染
 * children 就是一个函数， 不管匹配还是不匹配都会渲染
*/
export default class NavLink extends Component {
  static contextType = Context;
  render(){
    let {to, exact = false} = this.props;
    if(typeof to === 'object') {
      to = to.pathname;
    };
    let pathname = this.context.location.pathname; // 地址栏地址
    let reg = pathToRegexp(to,[], {end: exact});
    let r = pathname.match(reg);
    return (
      <Link {...this.props} className={r? 'active': ''} >
         {this.props.children}
      </Link>
    )
  }
}