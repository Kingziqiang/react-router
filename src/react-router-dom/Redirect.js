import React, { Component } from 'react'
import Context from './context';
export default class Redirect extends Component{
  static contextType = Context;
  render(){
    this.context.history.push(this.props.to);
    return null;
  }
}