import React, { Component } from 'react'
import Context from './context';
import { pathToRegexp } from 'path-to-regexp';
export default class Switch extends Component{
  static contextType = Context;
  render(){
    let pathname = this.context.location.pathname;
    for(let i=0;i<this.props.children.length;i++){
        let child = this.props.children[i];
        let {path='/',component:Component,exact=false} = child.props;
        let regxp = pathToRegexp(path,[],{end:exact});
        let result = pathname.match(regxp);
        if(result){
            return child;
        }
    }
    return null;

    // let pathname = this.context.location.pathname;
    // let childrens = this.props.children;
    // for (let i = 0; i < childrens.length; i++) {
    //   let child = childrens[i];
    //   console.log(child);
    //   let {to='/', component:Component, exact=false} = child.props;
    //   let regexp = pathToRegexp(to,[],{end: exact});
    //   let r = pathname.match(regexp)
    //   if(r) {
    //     console.log(child);
    //     return child;
    //   }
    // }
    
    // return null;
  }
}