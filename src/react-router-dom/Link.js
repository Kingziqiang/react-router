import React, {Component} from 'react';
import Context from './context';
export default class Link extends Component {
  static contextType = Context;
  render(){
    return (
      <a {...this.props} onClick={()=>this.context.history.push(this.props.to)}>
      {this.props.children}
      </a>
    )
  }
}