import React, { Component } from 'react'
import Context from './context'
export default class HashRouter extends Component {
  localState = undefined
  state = {
    location: {
      // hash: window.location.hash.match(/#[# | \w]+/)[0] || '',
      // pathname:/^#(.+)\?$/g.exec(window.location.hash)[1]|| '/',
      // search: /\?[^#?]+=[^#?]+/.exec(window.location.hash)[0] || '',
      // state: null
      pathname: window.location.hash.slice(1),
      state: this.localState,
    },
  }
  componentWillMount() {
    window.addEventListener('hashchange', () => {
      this.setState({
        location: {
          ...this.state.location,
          pathname: window.location.hash.slice(1),
          // hash: window.location.hash.match(/\#[# | \w]+/)[0] || '',
          // pathname:/^#(.+)\?$/g.exec(window.location.hash)[1]|| '/',
          // search: /\?[^#?]+=[^#?]+/.exec(window.location.hash)[0] || '',
          state: this.localState,
        },
      })
      console.log(this.state)

      window.location.hash = window.location.hash || ''
    })
  }
  render() {
    let that = this
    let value = {
      location: this.state.location,
      history: {
        location: this.state.location,
        push(to) {
          if (that.getMessage) {
            let allow = window.confirm(
              that.getMessage(typeof to === 'object' ? to : { pathname: to })
            )
            if (!allow) return
          }
          if (typeof to === 'object') {
            let { pathname, state } = to
            that.localState = state
            window.location.hash = pathname
          } else {
            window.location.hash = to
          }
        },
        block(message) {
          that.getMessage = message
        },
        unblock() {
          that.getMessage = null
        },
      },
    }
    return (
      <Context.Provider value={value}>{this.props.children}</Context.Provider>
    )
  }
}
