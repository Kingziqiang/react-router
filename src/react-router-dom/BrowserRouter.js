import React, {Component} from 'react'
import Context from "./context";
export default class BrowserRouter extends Component {
  state = {
    location: {
      pathname: '/',
      state: null
    },
  }
  componentWillMount(){
   (function(history){
      let oldPushState = history.pushState;
      window.history.pushState = function(state,title,path){
        if( typeof window.onpushstate === 'function') {
          window.onpushstate(state,path)
        }
        oldPushState.apply(history, arguments);
      }
    })(window.history);
    window.onpopstate=(event)=>{
      this.setState({
        location: {
          ...this.state.location,
          pathname: document.location.pathname,
          state:event.state
        }
      })
    };
    window.onpushstate = (state,pathname)=>{
      this.setState({
        location: {
          ...this.state.location,
          pathname,
          state
        }
      })
    }
    
  }
  render(){
    let that = this;
    let value = {
      location: this.state.location,
      history: {
        push(to){
          if(that.getMessage) {
            let allow = window.confirm(that.getMessage(typeof to === 'object' ? to : { pathname: to }))
            if(!allow)return;
          }
          if(typeof to === 'object') {
            let {pathname, state} = to;
            window.history.pushState(state, '', pathname);
          }else {
            window.history.pushState(null, '', to);
          }
        },
        block(message) {
          that.getMessage = message;
        },
        unblock(){
          that.getMessage = null;
        }
      }
    };
    return (
      <Context.Provider value={value}>
        {this.props.children}
      </Context.Provider>
    )
  }
}