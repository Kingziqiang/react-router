import React, { Component } from 'react'
import Context from './context'
import { pathToRegexp } from 'path-to-regexp'
export default class Route extends Component {
  static contextType = Context
  render() {
    let {
      path = '/',
      component: Component,
      exact = false,
      render,
      children,
    } = this.props
    let pathname = this.context.location.pathname // 获取地址栏中的parthname
    let keys = []
    let regexp = pathToRegexp(path, keys, { end: exact })
    let result = pathname.match(regexp)
    let props = {
      location: this.context.location,
      history: this.context.history,
    }
    if (result) {
      let [url, ...values] = result
      keys = keys.map((item) => item.name)
      let params = keys.reduce((memo, name, index) => {
        memo[name] = values[index]
        return memo
      }, {})
      let match = {
        url, // 匹配的路径(/user/detail/1594864449809)
        path, // 属性对象上的path(/user/detail/:id)
        params,
        path: '',
        isExact: url === pathname, // 地址栏路径（看看匹配的路径和完整路由是否完全相等）
      }
      props.match = match
      if (Component) {
        // 组件直接渲染(需要匹配路径)
        return <Component {...props} /> // 会把路由对象通过属性对象传递给真正的组件
      } else if (render) {
        return render(props) // render渲染可以添加逻辑（需要匹配路径）
      } else if (children) {
        return children(props)
      }
    } else {
      if (children) {
        return children(props)
      } else {
        return null
      }
    }
  }
}
