import React from 'react'
import { Route } from './'

export default function (OldComponent) {
  return (props) => {
    // props 为此组件的属性对象
    return (
      <Route
        // routerProps--> {location, history, match}
        render={(routerProps) => <OldComponent {...props} {...routerProps} />}
      />
    )
  }
}
