import React, {Component} from 'react';
import Context from './context'
export default class Prompt extends Component {
  static contextType = Context;
  componentWillUnmount(){
    this.history.unblock();
  }
  render(){
    this.history = this.context.history;
    const {when, message} = this.props;
    if(when) {
      this.history.block(message);
    }else {
      this.history.block(null);
    }
    return null;
  }
}