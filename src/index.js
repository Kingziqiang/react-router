import React from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  Switch,
  NavLink,
} from './react-router-dom'
import Home from './components/Home'
import User from './components/User'
import Profile from './components/Profile'
import Login from './components/Login'
import Header from './components/Header'
import Protected from './components/Protected'
ReactDOM.render(
  <Router>
    <div>
      <Header />
      <NavLink exact to="/">
        Home
      </NavLink>
      <NavLink to="/user">User</NavLink>
      <NavLink to="/profile">Profile</NavLink>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/user" component={User} />
        {/* <Route path="/profile" component={Profile} /> */}
        <Route path="/login" component={Login} />
        <Protected path="/profile" component={Profile}></Protected>
        <Redirect to="/" />
      </Switch>
    </div>
  </Router>,
  document.getElementById('root')
)
