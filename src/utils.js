export const Users = {
  list() {
      let userStr = localStorage.getItem('users');
      return userStr ? JSON.parse(userStr) : [];
  },
  add(user) {
      let users = Users.list();
      users.push(user);
      localStorage.setItem('users', JSON.stringify(users));
  },
  find(id) {
      let users = Users.list();
      return users.find(user => user.id === id);
  }
}